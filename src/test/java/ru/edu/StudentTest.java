package ru.edu;

import org.junit.Test;
import ru.edu.model.Student;
import static org.junit.Assert.*;

import java.sql.Date;
import java.time.Instant;
import java.util.UUID;

public class StudentTest {

    Student firstStudent;
    Student secondStudent;
    Student thirdStudent;

    Date firstDate = new Date(Instant.now().getEpochSecond());
    Date secondDate = new Date(Instant.now().getEpochSecond());
    Date thirdDate = new Date(Instant.now().getEpochSecond());

    @Test
    public void test() {
        firstStudent = new Student("Ivan", "Ivanov",
                firstDate, false, "male");

        assertNotNull(firstStudent);
        assertEquals("Ivan", firstStudent.getFirstName());
        assertEquals("Ivanov", firstStudent.getLastName());
        assertEquals(firstDate, firstStudent.getBirthDate());
        assertFalse(firstStudent.isGraduated());
        assertEquals("male", firstStudent.getGender());



        secondStudent = new Student("Maria", "Chuanovna",
                secondDate, true, "female");

        assertNotNull(secondStudent);
        assertEquals("Maria", secondStudent.getFirstName());
        assertEquals("Chuanovna", secondStudent.getLastName());
        assertEquals(secondDate, secondStudent.getBirthDate());
        assertTrue(secondStudent.isGraduated());
        assertEquals("female", secondStudent.getGender());



        thirdStudent = new Student("", "",
                thirdDate, true, "");

        thirdStudent.setId(UUID.randomUUID());
        thirdStudent.setFirstName("TestName");
        thirdStudent.setLastName("TestLastName");
        thirdStudent.setBirthDate(thirdDate);
        thirdStudent.setGraduated(true);
        thirdStudent.setGender("TestGender");

        assertNotNull(thirdStudent);
        assertEquals("TestName", thirdStudent.getFirstName());
        assertEquals("TestLastName", thirdStudent.getLastName());
        assertEquals(thirdDate, thirdStudent.getBirthDate());
        assertTrue(thirdStudent.isGraduated());
        assertEquals("TestGender", thirdStudent.getGender());
    }
}
