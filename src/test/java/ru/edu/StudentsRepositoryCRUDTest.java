package ru.edu;

import org.junit.Before;
import org.junit.Test;
import ru.edu.model.Student;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;
import static ru.edu.JdbcTest.JDBC_H_2_LOCAL_DB_DB;


public class StudentsRepositoryCRUDTest {

    Student firstStudent;
    Student secondStudent;
    Student thirdStudent;

    Date firstDate = new Date(Instant.now().getEpochSecond());
    Date secondDate = new Date(Instant.now().getEpochSecond());
    Date thirdDate = new Date(Instant.now().getEpochSecond());

    @Before
    public void setUp() {

        firstStudent = new Student();

        firstStudent = new Student("Ivan", "Ivanov",
                firstDate, false, "male");

        secondStudent = new Student("Maria", "Chuanovna",
                secondDate, true, "female");

        thirdStudent = new Student("", "",
                thirdDate, true, "");
    }

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection(JDBC_H_2_LOCAL_DB_DB, "sa", "");
    }

    @Test
    public void createTest() throws SQLException {
        StudentsRepositoryCRUDImpl crud = new StudentsRepositoryCRUDImpl(getConnection());
        UUID createdId = crud.create(firstStudent);

        Student newStudent = crud.selectById(createdId);

        assertNotNull(firstStudent);
        assertEquals(createdId, newStudent.getId());
        assertEquals(firstStudent.getFirstName(), newStudent.getFirstName());
        assertEquals(firstStudent.getLastName(), newStudent.getLastName());
        assertNotNull(firstStudent.getBirthDate());
        assertEquals(firstStudent.isGraduated(), newStudent.isGraduated());
        assertEquals(firstStudent.getGender(), newStudent.getGender());
    }

    @Test
    public void updateTest() throws SQLException {
        StudentsRepositoryCRUDImpl crud = new StudentsRepositoryCRUDImpl(getConnection());
        UUID createdId = crud.create(firstStudent);

        firstStudent.setId(createdId);
        firstStudent.setFirstName("TestName");
        firstStudent.setLastName("TestLastName");
        firstStudent.setBirthDate(new Date(Instant.now().getEpochSecond()));
        firstStudent.setGraduated(false);
        firstStudent.setGender("female");

        crud.update(firstStudent);

        assertNotNull(firstStudent);
        assertEquals(createdId, firstStudent.getId());
        assertEquals("TestName", firstStudent.getFirstName());
        assertEquals("TestLastName", firstStudent.getLastName());
        assertFalse(firstStudent.isGraduated());
        assertEquals("female", firstStudent.getGender());
    }

    @Test
    public void selectAllTest() throws SQLException {
        StudentsRepositoryCRUDImpl crud = new StudentsRepositoryCRUDImpl(getConnection());
        crud.create(firstStudent);

        List<Student> students = crud.selectAll();
        System.out.println(students);

        assertNotNull(students);
    }

    @Test
    public void removeTest() throws SQLException {
        StudentsRepositoryCRUDImpl crud = new StudentsRepositoryCRUDImpl(getConnection());

        List<UUID> uuid = new ArrayList<>();
        UUID createdFirstId = crud.create(firstStudent);
        UUID createdSecondId = crud.create(secondStudent);
        UUID createdThirdId = crud.create(thirdStudent);

        uuid.add(createdFirstId);
        uuid.add(createdSecondId);
        uuid.add(createdThirdId);

        assertNotNull(uuid);

        assertEquals(3, crud.remove(uuid));
    }


}