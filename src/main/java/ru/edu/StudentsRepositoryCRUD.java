package ru.edu;

import ru.edu.model.Student;

import java.util.List;
import java.util.UUID;

/**
 * Объект содержащий основные методы CRUD.
 */
public interface StudentsRepositoryCRUD {

    /**
     * Создание записи в БД.
     * id у student должен быть null, иначе требуется вызов update.
     * id генерируем через UUID.randomUUID()
     *
     * @param student - запись-студент
     * @return сгенерированный UUID
     */
    UUID create(Student student);

    /**
     * Получение записи по id из БД.
     *
     * @param id - id записи
     * @return - запись по id из БД.
     */
    Student selectById(UUID id);

    /**
     * Получение всех записей из БД.
     *
     * @return - все записи из БД.
     */
    List<Student> selectAll();

    /**
     * Обновление записи в БД.
     *
     * @param student - запись-студент.
     * @return количество обновленных записей
     */
    int update(Student student);

    /**
     * Удаление указанных записей по id.
     *
     * @param idList - список id для удаления
     * @return количество удаленных записей
     */
    int remove(List<UUID> idList);
}
