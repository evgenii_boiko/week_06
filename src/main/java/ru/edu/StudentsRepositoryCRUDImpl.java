package ru.edu;

import ru.edu.model.Student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class StudentsRepositoryCRUDImpl implements StudentsRepositoryCRUD {


    /**
     * Выражение SQL.
     * Вставка записи.
     */
    public static final String INSERT_STUDENT = "INSERT INTO STUDENTS"
            + "(id, first_name, last_name, birth_date, is_graduated, gender)"
            + "VALUES(?, ?, ?, ?, ?, ?)";

    /**
     * Выражение SQL.
     * Выбор записи по id.
     */
    public static final String SELECT_BY_ID = "SELECT *"
            + "FROM STUDENTS WHERE id=?";

    /**
     * Выражение SQL.
     * Выбор всех записей.
     */
    public static final String SELECT_ALL = "SELECT *"
            + "FROM STUDENTS";

    /**
     * Выражение SQL.
     * Обновление требуемых записей.
     */
    public static final String UPDATE_RECORDS = "UPDATE "
            + "STUDENTS SET first_name=?,"
            + "last_name=?,"
            + "birth_date=?,"
            + "is_graduated=?,"
            + "gender=?"
            + "WHERE id=?";

    /**
     * Выражение SQL.
     * Удаление записи.
     */
    public static final String REMOVE_RECORDS = "DELETE "
            + "FROM STUDENTS WHERE id=?";



    /**
     * Подключение к БД.
     */
    private Connection connection;

    /**
     * В данном конструкторе получаем подключение извне.
     *
     * @param connectionInc - подключение
     */
    public StudentsRepositoryCRUDImpl(final Connection connectionInc) {
        this.connection = connectionInc;
    }

    /**
     * Создание записи в БД.
     * id у student должен быть null, иначе требуется вызов update.
     * id генерируем через UUID.randomUUID()
     *
     * @param student - объект-студент, задаваемый
     *                в классе StudentsRepositoryCRUDTest
     * @return сгенерированный UUID
     */
    @Override
    public UUID create(final Student student) {
        UUID id = UUID.randomUUID();
        int insertedRows = execute(INSERT_STUDENT,
                id.toString(),
                student.getFirstName(),
                student.getLastName(),
                student.getBirthDate(),
                student.isGraduated(),
                student.getGender());

        if (insertedRows == 0) {
            throw new IllegalStateException("Values are not inserted");
        }
        return id;
    }

    private int execute(final String sql, final Object... values) {

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            for (int i = 0; i < values.length; i++) {
                statement.setObject(i + 1, values[i]);
            }

            return statement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Получение записи по id из БД.
     *
     * @param id - id записи
     * @return - запись по id из БД.
     */
    @Override
    public Student selectById(final UUID id) {
        List<Student> query = query(SELECT_BY_ID, id.toString());
        return query.get(0);
    }

    private List<Student> query(final String sql, final Object... values) {

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            for (int i = 0; i < values.length; i++) {
                statement.setObject(i + 1, values[i]);
            }
            ResultSet resultSet = statement.executeQuery();

            List<Student> result = new ArrayList<>();
            while (resultSet.next()) {
                result.add(map(resultSet));
            }
            return result;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    private Student map(final ResultSet resultSet) throws SQLException {
        Student student = new Student();
        student.setId(UUID.fromString(resultSet.getString("id")));
        student.setFirstName(resultSet.getString("first_name"));
        student.setLastName(resultSet.getString("last_name"));
        student.setBirthDate(resultSet.getDate("birth_date"));
        student.setGraduated(resultSet.getBoolean("is_graduated"));
        student.setGender(resultSet.getString("gender"));
        return student;
    }

    /**
     * Получение всех записей из БД.
     *
     * @return - все записи из БД.
     */
    @Override
    public List<Student> selectAll() {
        List<Student> query = query(SELECT_ALL);
        return query;
    }

    /**
     * Обновление записи в БД.
     *
     * @param student - запись-студент.
     * @return количество обновленных записей
     */
    @Override
    public int update(final Student student) {
        UUID id = student.getId();
        int updatedRecords = execute(UPDATE_RECORDS,
                student.getFirstName(),
                student.getLastName(),
                student.getBirthDate(),
                student.isGraduated(),
                student.getGender(),
                id.toString());
        return updatedRecords;
    }

    /**
     * Удаление указанных записей по id.
     *
     * @param idList - список id для удаления
     * @return количество удаленных записей
     */
    @Override
    public int remove(final List<UUID> idList) {
        int removedRecordsCount = 0;
        for (UUID uuid: idList) {
            removedRecordsCount  += execute(REMOVE_RECORDS, uuid.toString());
        }
        if (removedRecordsCount == 0) {
            throw new IllegalStateException("No records are removed");
        }
        return removedRecordsCount;
    }
}
