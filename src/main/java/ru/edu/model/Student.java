package ru.edu.model;

import java.util.Date;
import java.util.UUID;

/**
 * Класс, отражающий структуру хранимых в таблице полей.
 */
public class Student {

    /**
     * Первичный ключ.
     *
     * Рекомендуется генерировать его только внутри
     * StudentsRepositoryCRUD.create(),
     * иными словами до момента пока объект
     * не будет сохранен в БД, он не должен
     * иметь значение id.
     */
    private UUID id;

    /**
     * Имя студента.
     */
    private String firstName;

    /**
     * Фамилия студента.
     */
    private String lastName;

    /**
     * День рождения студента.
     */
    private Date birthDate;

    /**
     * Булево значение.
     * Выпустился ли студент.
     */
    private boolean isGraduated;

    /**
     * Пол студента.
     */
    private String gender;

    /**
     * Безаргументный конструктор студента.
     */
    public Student() {
    }

    /**
     * Конструктор с аргументами студента.
     *
     * @param firstNameInc - Имя студента.
     * @param lastNameInc - Фамилия студента.
     * @param birthDateInc - День рождения студента.
     * @param isGraduatedInc - Выпустился ли студент.
     * @param genderInc - Пол студента.
     */
    public Student(final String firstNameInc, final String lastNameInc,
                   final Date birthDateInc, final boolean isGraduatedInc,
                   final String genderInc) {
        this.firstName = firstNameInc;
        this.lastName = lastNameInc;
        this.birthDate = birthDateInc;
        this.isGraduated = isGraduatedInc;
        this.gender = genderInc;
    }

    /**
     * @return id - id записи.
     */
    public final UUID getId() {
        return id;
    }

    /**
     * @param idInc - id записи.
     */
    public final void setId(final UUID idInc) {
        this.id = idInc;
    }

    /**
     * @return firstName - Имя студента.
     */
    public final String getFirstName() {
        return firstName;
    }

    /**
     * @param firstNameInc - Имя студента.
     */
    public final void setFirstName(final String firstNameInc) {
        this.firstName = firstNameInc;
    }

    /**
     * @return lastName - Фамилия студента.
     */
    public final String getLastName() {
        return lastName;
    }

    /**
     * @param lastNameInc - Фамилия студента.
     */
    public final void setLastName(final String lastNameInc) {
        this.lastName = lastNameInc;
    }

    /**
     * @return birthDateInc - День рождения студента.
     */
    public final Date getBirthDate() {
        return birthDate;
    }

    /**
     * @param birthDateInc - День рождения студента.
     */
    public final void setBirthDate(final Date birthDateInc) {
        this.birthDate = birthDateInc;
    }

    /**
     * @return isGraduated - Выпустился ли студент.
     */
    public final boolean isGraduated() {
        return isGraduated;
    }

    /**
     * @param graduatedInc - Выпустился ли студент.
     */
    public final void setGraduated(final boolean graduatedInc) {
        isGraduated = graduatedInc;
    }

    /**
     * @return gender - Пол студента.
     */
    public final String getGender() {
        return gender;
    }

    /**
     * @param genderInc - Пол студента.
     */
    public final void setGender(final String genderInc) {
        this.gender = genderInc;
    }
}
